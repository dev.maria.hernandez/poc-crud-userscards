const functions = require('firebase-functions');
const express = require("express")
const app = express()
var admin = require("firebase-admin");

var serviceAccount = require("./poc-crud-firebase-adminsdk-w1moy-997d19bb94.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://poc-crud.firebaseio.com/"
});

const users = require('./users.js')
const cards = require('./cards')


app.use('/', users)
app.use('/', cards);

exports.mari = functions.https.onRequest(app)	
