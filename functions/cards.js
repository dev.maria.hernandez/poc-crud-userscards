const express = require("express")
const app = express()
const bodyParser = require("body-parser")
const router = express.Router()
var admin = require("firebase-admin");
const db = admin.database()

app.use(express.static('public'));
app.use(bodyParser.json());

//Get all cards
router.get("/cards/", async (req, res, next) => {
    const snapshot = await db.ref("/cards").once('value');
    res.send(snapshot)
})

//Create all cards
router.post("/cards/", async (req, res, next) =>{
    const newCard = req.body
    if(!newCard.number || !newCard.expirationDate || !newCard.cvv){
        return res.status(400).send("Invalid card!")
    }
    const snapshot = await db.ref("/cards").push(newCard)
    
    res.status(201).send(newCard)
})


//Get a single card
router.get("/cards/:cardId", async (req, res, next) => {
    const cardId = req.params.cardId
    var snapshot = await db.ref("/cards/"+cardId).once('value')
    if(snapshot === false){
        return res.status(404).send('Card not found')
    }  
    res.send(snapshot) 
    
})


//Update a card
router.put("/cards/:cardId", async (req, res, next) => {
    const cardId = req.params.cardId
    const updateCard = req.body
    var snapshot = await db.ref("/cards/"+cardId).once('value')
    if (!snapshot) {
        return res.status(404).send('Card not found')
    }
    snapshot = await db.ref("/cards/"+cardId).update(updateCard)
    res.send(updateCard)
})

//Delete a card
router.delete("/cards/:cardId", async (req, res, next) => {
    const cardId = req.params.cardId
    var snapshot = await db.ref("/cards/"+cardId).once("value")
    if (!snapshot) {
        return res.status(404).send('card not found')
    }
    
    snapshot = await db.ref("/users/").once("value")
        
    snapshot.forEach( dataUserSnapshot => {
        var oldCards = dataUserSnapshot.child("cards").val()
        var cardArray = oldCards.filter( cardID => cardId !== cardID)
        if(cardArray.length !== oldCards.length){
            db.ref("/users/"+dataUserSnapshot.key+"/cards").set(cardArray)
            console.log("User update: ", dataUserSnapshot.key)
        }
        console.log(oldCards)
        console.log(cardArray)
    });
    db.ref("/cards/"+cardId).remove()
    return res.status(204).send()
    
})

module.exports = router
