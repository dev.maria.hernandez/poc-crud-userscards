const express = require("express")
const app = express()
const router = express.Router()
var admin = require("firebase-admin")
const bodyParser = require("body-parser")
const db = admin.database()

app.use(express.static('public'));
app.use(bodyParser.json())


//create all users
router.post("/users/", async (req, res, next) =>{
    const newUser = req.body
    const cardsAll = req.body.cards
    if(!newUser.name || !newUser.lastname || !newUser.age || !newUser.cards){
        return res.status(400).send("Invalid users!")
    }
    var snapshot = await db.ref("/cards").once('value') 
    const verificate = cardsAll.every( cardId => snapshot.child(cardId).exists())

    if(!verificate){
        return res.status(400).send("Invalid card!")
    }
    
    snapshot = await db.ref("/users").push(newUser)
    res.status(201).send(verificate)
})

//Get all users
router.get("/users/", async (req, res, next) => {
    const snapshot = await db.ref("/users").once('value');
    res.send(snapshot)
})

//Get a single user
router.get("/users/:userId", async (req, res, next) => {
    const userId = req.params.userId
    const snapshot = await db.ref("/users/"+userId).once('value')
    if(snapshot === false){
        return res.status(404).send('User not found')
    }
    res.send(snapshot)

    
})

//Get a single user with all card
router.get("/users/:userId/cards", async (req, res, next) =>{
    const cardsAll = req.params.cards
    const userId = req.params.userId
    var snapshot = await db.ref("/users/"+userId).once('value')
    if(!snapshot){
        return res.status(404).send('User not found');
    }    
    snapshot = await db.ref("/users/" + userId + "/cards").once('value')
    res.send(snapshot)
})

//Update a card
router.put("/users/:userId", async (req, res, next) => {
    const userId = req.params.userId
    const updateUser = req.body
    var snapshot = await db.ref("/users/"+userId).once("value")
    if (!snapshot) {
        return res.status(404).send('User not found')
    }
    snapshot = await db.ref("/users/"+userId).update(updateUser)
    res.send(updateUser)
})

//Delete a card
router.delete("/users/:userId", async (req, res, next) => {
    const userId = req.params.userId
    var snapshot = await db.ref("/users/"+userId).once("value")
    if (!snapshot) {
        return res.status(404).send('User not found')
    }
    snapshot = db.ref("/users/"+userId).remove()
    return res.status(404).send()
})



module.exports = router
    

